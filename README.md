# PwaTpNote
# I - Workbox



### Liste des fonctionnalités offertes
- Hors ligne (avec la gestion du cache)
- Mise en cache
- Notifications push
- Notification
- Gestion des erreurs
- Sécurité
- Performance (avec les taches de fond)
- Mises à jour automatiques
- Cache vidéo et audio (perf et qualité)
- Possibilité d'utilisation de plugin
- Gestion des réseaux lent
- Accéder au cache depuis la fenêtre 


### Méthodes de cache et utilisations possibles

* Cache prioritaire et réseau si pas de cache
  1. Check dans le cache si l'élément existe, si oui il est retourné, sinon le réseau est utilisé et l'élément est retourné
  2. Une fois l'élément retourné par le réseau il est set dans le cache

* Réseau prioritaire et cache si pas de réseau  (le cache sert de backup)
  1. Le réseau est requêté puis l'élément retourné est mis en cache
  2. Si pas de réseau  on retourne le dernier élément du cache
* Cache
  - On met en cache tout élément de l'app pendant les première requêtes
* Réseau
  - Pas de mise en cache, le réseau est directement requeté
### Uses cases à intégrer dans le projet doctoliberal
* Cache des rdv en local
  - mise en cache des rdv avec la méthode du réseau prioritaire pour avoir un backup des rdv en local si jamais il y a une perte de connexion
* Calcul des trajets
  - Calcul des trajets en parallèle pour permettre aux professionnels de les consulter  au fur et à mesure
* Gestion des erreurs réseau
* Notifications
* Notification Push

# II - Page d'incitation à l'installation de PWA

1 ) Créer un composant (React / Vue / angular / HTML+CSS) pour inciter et surtout guider un utilisateur à installer la PWA
- navigateur nécessaire
- compatibilité du device
- capacités supplémentaires, etc...
- 
2 ) Expliquer en quoi il serait interessant de mettre en place une telle page

![Site](img.png)
Url du site : https://pwa-tp-note.web.app/
